﻿using System;
using Quartz;
using Quartz.Impl;
using System.Drawing;
using System.Windows.Forms;
using Windows.System.UserProfile;
using Windows.Storage;

namespace LiveSunPro
{
	public partial class App : System.Windows.Application
	{
		private NotifyIcon notifyIcon;

		private IScheduler sched;
		private ISchedulerFactory schedFact;

		public App()
		{
			Configuration.readConfig();

			notifyIcon = new NotifyIcon();

			schedFact = new StdSchedulerFactory();
			sched = schedFact.GetScheduler();

			ContextMenu contextMenu = new ContextMenu();

			MenuItem menuRefresh = new MenuItem();
			menuRefresh.Index = 0;
			menuRefresh.Text = "R&efresh";
			menuRefresh.Click += MenuRefresh_Click;

			MenuItem menuExit = new MenuItem();
			menuExit.Index = 1;
			menuExit.Text = "E&xit";
			menuExit.Click += MenuExit_Click;

			contextMenu.MenuItems.AddRange(new MenuItem[] { menuRefresh, menuExit });

			notifyIcon.Icon = new Icon("icon.ico");
			notifyIcon.ContextMenu = contextMenu;
			notifyIcon.Text = "LiveSun Pro";
			notifyIcon.Visible = true;

			notifyIcon.DoubleClick += new EventHandler(this.notifyIcon1_DoubleClick);
			//notifyIcon.Click += NotifyIcon_Click;
			createCron();

			Wallpaper.downloadPicture(Configuration.config["IMAGEURL"]);
			Wallpaper.setWallpaper();
		}

		private void createCron()
		{
			String jobName = "WallpaperJob";

			sched.Clear();
			sched.Start();

			IJobDetail job = JobBuilder.Create<WallpaperJob>().WithIdentity(jobName, "group").Build();

			ITrigger trigger = TriggerBuilder.Create()
				.WithIdentity("trigger", "group")
				//.WithCronSchedule("0 0,15,30,45 * * * ?")
				.WithCronSchedule(Configuration.config["SCHEDULE"])
				.ForJob(jobName, "group")
				.Build();

			sched.ScheduleJob(job, trigger);
		}

		private /*async*/ void MenuRefresh_Click(object sender, EventArgs e)
		{
			Configuration.readConfig();

//			Console.WriteLine("s");
//			//StorageFile img = StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Images/mozi.png")).GetResults();
//			StorageFile img = await StorageFile.GetFileFromPathAsync("E:\\rm\\App2\\App2\\Images\\mozi.png");
//			Console.WriteLine("a");
////#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
//			await LockScreen.SetImageFileAsync(img);
////#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
//			Console.WriteLine("d");

			createCron();

			Wallpaper.downloadPicture(Configuration.config["IMAGEURL"]);
			Wallpaper.setWallpaper();
		}

		private void MenuExit_Click(object sender, EventArgs e)
		{
			this.notifyIcon.Visible = false;
            Environment.Exit(0);

        }

		private void notifyIcon1_DoubleClick(object sender, EventArgs e)
		{
			Console.WriteLine("double");
		}

		private void NotifyIcon_Click(object sender, EventArgs e)
		{
			Console.WriteLine("click");
		}
	}
}
