﻿using System;
using System.Collections.Generic;
using System.IO;

namespace LiveSunPro
{
	public class Configuration
	{
		private static String configPath = "config.cfg";
		public static Dictionary<String, String> config = new Dictionary<String, String>();

		public static void readConfig()
		{
			config = new Dictionary<String, String>();

			string[] lines = File.ReadAllLines(configPath);

			foreach (string line in lines)
			{
				int separation = line.IndexOf('=');
				config.Add(line.Substring(0, separation), line.Substring(separation + 1));
			}
		}
	}
}
