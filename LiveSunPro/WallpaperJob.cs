﻿using Quartz;
using System;

namespace LiveSunPro
{
	public class WallpaperJob : IJob
	{
		void IJob.Execute(IJobExecutionContext context)
		{
			try
			{
				Wallpaper.downloadPicture(Configuration.config["IMAGEURL"]);
				Wallpaper.setWallpaper();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message + " -2- " + DateTime.Now.ToString());
				//throw;
			}
		}
	}

}
