﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;

namespace LiveSunPro
{
	public class Wallpaper
	{
		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern Int32 SystemParametersInfo(UInt32 action, UInt32 uParam, String vParam, UInt32 winIni);

		private static String path;
		private static bool isRunning = false;

		public static void downloadPicture(String url)
		{
			if (!isRunning)
			{
                try
                {
                    isRunning = true;
                    Console.WriteLine("Wallpaper download started " + DateTime.Now.ToString());
					
					path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "sunimg.jpg");

					using (WebClient client = new WebClient())
					{
						System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
						client.DownloadFile(url, path);
					}

					Console.WriteLine("Wallpaper download finished");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message + " -1- " + DateTime.Now.ToString());
                    //throw;
                }

                isRunning = false;
            }
		}

		public static void setWallpaper()
		{
			if (!isRunning && new FileInfo(path).Length != 0)
			{
				isRunning = true;
				RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);

				// set wallpaper to "fit" style
				key.SetValue(@"WallpaperStyle", 6.ToString());
				key.SetValue(@"TileWallpaper", 0.ToString());

				SystemParametersInfo(0x14, 0, path, 0x01 | 0x02);
				isRunning = false;
			}
		}
	}

}
